package domain

import "time"

type User struct {
	Id          int       `db:"id" json:"id"`
	UserName    string    `db:"user_name" json:"user_name"`
	PassWord    string    `db:"pass_word" json:"pass_word"`
	PhoneNumber string    `db:"phone_number" json:"phone_number"`
	CreatedAt   time.Time `db:"created_at" json:"created_at"`
	UpdatedAt   time.Time `db:"updated_at" json:"updated_at"`
}
