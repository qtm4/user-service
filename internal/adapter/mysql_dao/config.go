package mysql_dao

import (
	"time"
)

const (
	KDefaultTimeout = 30 * time.Second
)
