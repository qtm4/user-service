package user_biz

import (
	"back-end/user-service/internal/adapter/mysql_dao"
	"back-end/user-service/internal/core/domain"
	"context"
	"duynd2002/gokits/libs/ilog"
	csql "duynd2002/gokits/libs/storage/sql-client"
	"errors"
)

type UserBiz struct {
	userDB *mysql_dao.MyPointMysqlDAO
}

func NewUserBiz() *UserBiz {
	userMySQL := csql.NewBaseMysqlDAO("mysql_user_service")
	t := &UserBiz{
		userDB: mysql_dao.NewMyPointMysqlDAO(userMySQL),
	}
	return t
}

func (biz *UserBiz) CreateUser(ctx context.Context, user *domain.User) error {
	return biz.userDB.CreateUser(ctx, user)
}
func (biz *UserBiz) GetUsers(ctx context.Context) (*[]domain.User, error) {
	return biz.userDB.GetUsers(ctx)
}
func (biz *UserBiz) GetUserByID(ctx context.Context, id int) (*domain.User, error) {
	err := biz.checkUserID(ctx, id)
	if err != nil {
		return nil, err
	}
	return biz.userDB.GetUserByID(ctx, id)
}
func (biz *UserBiz) UpdateUser(ctx context.Context, user *domain.User) error {
	err := biz.checkUserID(ctx, user.Id)
	if err != nil {
		return err
	}
	return biz.userDB.UpdateUser(ctx, user)
}
func (biz *UserBiz) DeleteUser(ctx context.Context, id int) error {
	err := biz.checkUserID(ctx, id)
	if err != nil {
		return err
	}
	return biz.userDB.DeleteUser(ctx, id)
}

// checkUserID checks if the given user ID exists in the database.
//
// ctx: the context of the function call.
// id: the user ID to check.
// error: returns an error if the user ID is not found.
func (biz *UserBiz) checkUserID(ctx context.Context, id int) error {
	userIds, err := biz.userDB.GetListIdUser(ctx)
	if err != nil {
		return err
	}

	found := false
	for _, userID := range *userIds {
		if userID == id {
			found = true
			break
		}
	}

	if !found {
		ilog.Error("Không tìm thấy id người dùng")
		return errors.New("Không tìm thấy id người dùng")
	}
	return nil
}
