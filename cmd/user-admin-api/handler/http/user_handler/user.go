package user_handler

import (
	"back-end/user-service/internal/core/biz/user_biz"
	"back-end/user-service/internal/core/domain"
	"duynd2002/gokits/libs/ilog"

	"github.com/gofiber/fiber/v2"
)

type UserHandler struct {
	biz *user_biz.UserBiz
}

func NewUserHandler(UserBiz *user_biz.UserBiz) *UserHandler {
	return &UserHandler{biz: UserBiz}
}

func (t *UserHandler) CreateUser(ctx *fiber.Ctx) error {
	var user domain.User
	if err := ctx.BodyParser(&user); err != nil {
		ilog.Error("Error body parser: ", err)
		return ctx.Status(fiber.StatusBadRequest).JSON(fiber.Map{"error": "Body không hợp lệ"})
	}
	if err := t.biz.CreateUser(ctx.Context(), &user); err != nil {
		return ctx.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}
	return ctx.Status(fiber.StatusCreated).JSON(fiber.Map{"message": "Tạo user thành công"})
}

func (t *UserHandler) GetUsers(ctx *fiber.Ctx) error {
	users, err := t.biz.GetUsers(ctx.Context())
	if err != nil {
		return ctx.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}
	return ctx.Status(fiber.StatusOK).JSON(users)
}

func (t *UserHandler) GetUserByID(ctx *fiber.Ctx) error {
	id, err := ctx.ParamsInt("id")
	if err != nil {
		ilog.Error("error param id: ", err)
		return ctx.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}
	user, err := t.biz.GetUserByID(ctx.Context(), id)
	if err != nil {
		return ctx.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}
	return ctx.Status(fiber.StatusOK).JSON(user)
}

func (t *UserHandler) UpdateUser(ctx *fiber.Ctx) error {
	var user domain.User
	id, err := ctx.ParamsInt("id")
	if err != nil {
		ilog.Error("error param id: ", err)
		return ctx.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}
	user.Id = id
	if err := ctx.BodyParser(&user); err != nil {
		ilog.Error("Error body parser: ", err)
		return ctx.Status(fiber.StatusBadRequest).JSON(fiber.Map{"error": "Body không hợp lệ"})
	}
	if err := t.biz.UpdateUser(ctx.Context(), &user); err != nil {
		return ctx.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}
	return ctx.Status(fiber.StatusOK).JSON(fiber.Map{"message": "Cập nhật user thành công"})
}

func (t *UserHandler) DeleteUser(ctx *fiber.Ctx) error {
	id, err := ctx.ParamsInt("id")
	if err != nil {
		ilog.Error("error param id: ", err)
		return ctx.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}
	if err := t.biz.DeleteUser(ctx.Context(), id); err != nil {
		return ctx.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}
	return ctx.Status(fiber.StatusOK).JSON(fiber.Map{"message": "Xóa user thành công"})
}
