package main

import (
	"flag"

	"duynd2002/gokits/app"
	csql "duynd2002/gokits/libs/storage/sql-client"
)

var (
	flagconf string
)

func init() {
	flag.StringVar(&flagconf, "conf", "./configs/config.yml", "config path, eg: -conf config.json")
}

// @title Order API Mobile documentation
// @version 1.0.0
// @contact.name   TrungPV
// @contact.email  trungpv@i-com.vn
// @schemes https
// @host api.sandbox.mypoint.com.vn
// @BasePath /8854/gup2start/
func main() {
	flag.Parse()

	app.InitServer(flagconf)

	csql.InstallSQLClientManager()

	s, err := wireApp()
	if err != nil {
		panic(err)
	}

	app.DoInstance(s)
}
