//go:build !wireinject
// +build !wireinject

package main

import (
	"back-end/user-service/cmd/user-admin-api/handler/http/user_handler"
	"back-end/user-service/cmd/user-admin-api/server"
	"back-end/user-service/internal/core/biz/user_biz"
	"net/http"
)

// Injectors from wire.go:

func wireApp() (*server.UserServer, error) {
	userBiz := user_biz.NewUserBiz()
	userHandler := user_handler.NewUserHandler(userBiz)
	httpServer := &http.Server{}
	userServer := server.NewUserServer(userHandler, httpServer)
	return userServer, nil
}
