package mysql_dao

import (
	"back-end/user-service/internal/core/domain"
	"context"
	"duynd2002/gokits/libs/ilog"
	"duynd2002/gokits/libs/itimer"
	csql "duynd2002/gokits/libs/storage/sql-client"
	"time"

	"github.com/jmoiron/sqlx"
)

type MyPointMysqlDAO struct {
	*csql.BaseMysqlDAO
}

func NewMyPointMysqlDAO(base *csql.BaseMysqlDAO) *MyPointMysqlDAO {
	return &MyPointMysqlDAO{
		base,
	}
}

func (dao *MyPointMysqlDAO) CreateUser(ctx context.Context, user *domain.User) error {
	ilog.Info("+++++++++ CreateUser +++++++++")
	ctx, cancel := context.WithTimeout(ctx, KDefaultTimeout)
	defer cancel()
	user.CreatedAt = time.Now()
	user.UpdatedAt = time.Now()
	query := `
	INSERT INTO users
	(user_name, pass_word, phone_number, created_at, updated_at)
	VALUES
	(:user_name, :pass_word, :phone_number, :created_at, :updated_at)
	`
	_, err := sqlx.NamedExecContext(ctx, dao.GetDB(ctx), query, user)
	if err != nil {
		ilog.Error("error while insert user into table users: ", err)
		return err
	}
	return nil
}

func (dao *MyPointMysqlDAO) GetUsers(ctx context.Context) (*[]domain.User, error) {
	ilog.Info("+++++++++ GetUsers +++++++++")
	ctx, cancel := context.WithTimeout(ctx, KDefaultTimeout)
	defer cancel()
	users := []domain.User{}
	query := `SELECT * FROM users`
	rows, err := dao.GetDB(ctx).QueryContext(ctx, query)
	if err != nil {
		ilog.Error("error while get list user from table users: ", err)
		return nil, err
	}
	defer rows.Close()
	user := domain.User{}
	for rows.Next() {
		err := rows.Scan(
			&user.Id,
			&user.UserName,
			&user.PassWord,
			&user.PhoneNumber,
			&user.CreatedAt,
			&user.UpdatedAt,
		)
		if err != nil {
			ilog.Error("error while scan user: ", err)
			return nil, err
		}
		users = append(users, user)
	}
	return &users, nil
}

func (dao *MyPointMysqlDAO) GetUserByID(ctx context.Context, id int) (*domain.User, error) {
	ilog.Info("+++++++++ GetUserByID +++++++++")
	ctx, cancel := context.WithTimeout(ctx, KDefaultTimeout)
	defer cancel()
	query := `
	SELECT * FROM users WHERE id = ?`
	var user domain.User

	row := dao.GetDB(ctx).QueryRowxContext(ctx, query, id)
	err := row.Scan(&user.Id, &user.UserName, &user.PassWord, &user.PhoneNumber, &user.CreatedAt, &user.UpdatedAt)
	if err != nil {
		ilog.Error("error while scan user: ", err)
		return nil, err
	}
	return &user, nil
}

func (dao *MyPointMysqlDAO) UpdateUser(ctx context.Context, user *domain.User) error {
	ilog.Info("+++++++++ UpdateUser +++++++++")
	ctx, cancel := context.WithTimeout(ctx, KDefaultTimeout)
	defer cancel()
	var query = `UPDATE users SET user_name=?, pass_word=?, phone_number=?, updated_at=? WHERE id=?`
	_, err := dao.GetDB(ctx).ExecContext(ctx, query, user.UserName, user.PassWord, user.PhoneNumber, time.Now().Format(itimer.TIME_FORMAT_TIME), user.Id)
	if err != nil {
		ilog.Error("error while update user in table users: ", err)
		return err
	}
	return nil
}

func (dao *MyPointMysqlDAO) DeleteUser(ctx context.Context, id int) error {
	ilog.Info("+++++++++ DeleteUser +++++++++")
	ctx, cancel := context.WithTimeout(ctx, KDefaultTimeout)
	defer cancel()
	query := `DELETE FROM users WHERE id = ?`

	_, err := dao.GetDB(ctx).ExecContext(ctx, query, id)
	if err != nil {
		return err
	}
	return nil
}

func (dao *MyPointMysqlDAO) GetListIdUser(ctx context.Context) (*[]int, error) {
	ctx, cancel := context.WithTimeout(ctx, KDefaultTimeout)
	defer cancel()
	query := `SELECT id FROM users`
	var userIds []int
	rows, err := dao.GetDB(ctx).QueryContext(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		if err := rows.Scan(&id); err != nil {
			return nil, err
		}
		userIds = append(userIds, id)
	}
	return &userIds, nil
}
