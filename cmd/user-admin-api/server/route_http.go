package server

import (
	"github.com/gofiber/fiber/v2"
	"github.com/spf13/viper"
)

func (s *UserServer) setupHttpRoute() {

	router := fiber.New()
	router.Get("/admin/api/user/", s.userHandler.GetUsers)
	router.Get("/admin/api/user/:id", s.userHandler.GetUserByID)
	router.Post("/admin/api/user/", s.userHandler.CreateUser)
	router.Put("/admin/api/user/:id", s.userHandler.UpdateUser)
	router.Delete("/admin/api/user/:id", s.userHandler.DeleteUser)
	go func() {
		if err := router.Listen(viper.GetString("Server.Http.Port")); err != nil {
			panic(err)
		}
	}()
}
