package server

import (
	"net/http"

	"back-end/user-service/cmd/user-admin-api/handler/http/user_handler"
)

type UserServer struct {
	userHandler *user_handler.UserHandler
	httpServer  *http.Server
}

func NewUserServer(
	userHandler *user_handler.UserHandler,
	httpServer *http.Server) *UserServer {
	return &UserServer{
		userHandler: userHandler,
		httpServer:  httpServer,
	}
}

func (s *UserServer) Start() {
	s.setupHttpRoute()
	go s.httpServer.ListenAndServe() // Use ListenAndServe method of http.Server
}

func (s *UserServer) Stop() {
	// Implement graceful shutdown if needed
	// For example, s.httpServer.Shutdown(context.Background())
}
