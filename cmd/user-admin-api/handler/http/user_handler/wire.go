package user_handler

import "github.com/google/wire"

var ProviderSet = wire.NewSet(NewUserHandler)
