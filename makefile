
# run elk stack with docker
.PHONY: run-elk
run-elk:
	docker compose -f internal/elk/docker-compose.yml up -d

# down elk stack with docker
.PHONY: down-elk
down-elk:
	docker compose -f internal/elk/docker-compose.yml down

.PHONY: update-elk	
update-elk:
	docker compose -f internal/elk/docker-compose.yml up -d --force-recreate
# generate wire
.PHONY: generate
generate:
	go mod tidy
	go get github.com/google/wire/cmd/wire@latest
	go generate ./...

# run alerting-service
.PHONY: run-user-admin
run-user-admin:
	go run ./cmd/user-admin-api/ -conf ./configs/configs.yml
