//go:build wireinject
// +build wireinject

package main

import (
	"back-end/user-service/cmd/user-admin-api/server"

	"duynd2002/gokits/shared/core/biz/auth"

	"github.com/google/wire"

	httpserver "duynd2002/gokits/libs/transport/http"
)

func wireApp() (*server.userServer, error) {
	panic(wire.Build(
		httpserver.ProviderSet,
		auth.ProviderSet,
		userHandler.ProviderSet,
		server.ProviderSet,
		user_biz.ProviderSet,
	))
}
